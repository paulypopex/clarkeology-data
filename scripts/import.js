#!/usr/bin/env node

'use strict'

const mysql = require('mysql')
const { htmlify, limit, quote, ymlsafe } = require('./tools')

const LIMIT = process.argv[2] || limit

const config = {
  host: process.env.MYSQL_HOST || 'localhost',
  user: process.env.MYSQL_USER || 'root',
  password: process.env.MYSQL_PASS,
  database: 'clarkeology',
  connectTimeout: 60000,
  insecureAuth: true
}

const connection = mysql.createConnection(config)
connection.connect()

const venuePermalink = row => `/v/${row && row.venueID}/`
const tagify = tags => String(tags).split(' ').filter(Boolean).map(tag => tag.replace(/\+/g, ' '))

connection.query(`select * from venue order by venueID desc limit ${LIMIT}`, (err, result) => {
  if (err) throw err
  result.forEach(row => {
    const title = row.venueName || row.venueID
    const address = ('' + row.venueAddress).replace(/(,\s*)+/g, ', ')
    // const tags = extractTagArray(row.venueName).concat(tagify(row.tags))
    const tags = tagify(row.tags)
    if (/restaurant/.test(row.venueName)) tags.push('restaurant')
    if (row.archived) tags.push('closed')

    console.log(`v${row.venueID}:`)
    console.log(`  name: ${quote(title)}`)
    if (row.venueDescription) console.log(`  description: ${quote(htmlify(row.venueDescription || ''))}`)
    console.log(`  address: ${quote(address)}`)
    console.log(`  permalink: ${venuePermalink(row)}`)
    if (row.venuePhone) console.log(`  phone: ${quote(row.venuePhone)}`)
    if (row.venueDirections) console.log(`  directions: ${quote(ymlsafe(htmlify(row.venueDirections)))}`)
    if (row.venueHistory) console.log(`  history: ${quote(ymlsafe(htmlify(row.venueHistory)))}`)
    if (row.archived) console.log('  archived: 1')
    if (row.latitude) console.log(`  lat: ${row.latitude}`)
    if (row.latitude) console.log(`  lon: ${row.longitude}`)
    if (row.rating) console.log(`  rating: ${row.venueRating}`)
    if (row.lastRating) console.log(`  lastRating: ${row.lastVenueRating}`)
    if (row.attended) console.log('  attended: 1')
    if (row.venueCloneOf) console.log(`  redirectTo: /v${row.venueCloneOf}/`)
    if (row.tags) console.log(`  tags: [${tags.map(quote).join(', ')}]`)
  })
  process.exit(0)
})
