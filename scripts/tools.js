const fs = require('fs')
const path = require('path')
const mkdirp = require('mkdirp')
const configFile = path.resolve(process.env.HOME, 'lastfm.json')

const MINTAGLENGTH = 2

exports.limit = 10

exports.getConfig = () => {
  return getOptionalJSONFile(configFile)
}

const getOptionalJSONFile = exports.getOptionalJSONFile = file => {
  try {
    return require(file)
  } catch (e) {
    return {}
  }
}

exports.setConfig = (config, callback) => {
  return setJSONFile(configFile, config, callback)
}

const setJSONFile = exports.setJSONFile = (file, data, callback) => {
  fs.writeFile(file, JSON.stringify(data, null, 2), err => {
    callback(err ? `🙀 chmod +w ${file} and try again` : null)
  })
}

const printable = exports.printable = tag => ('' + (tag || ''))
  .replace(/[^ -~]+/g, '')

const urlify = exports.urlify = tag => ('' + (tag || '')).toLowerCase()
  .trim()
  .replace(/[^\w-]+/g, '+')
  .replace(/^the\+/, '')

const removeCommonWords = exports.removeCommonWords = tag => ('' + tag)
  .replace(/^\w{16,}/i, '') // too long to be a tag
  .replace(/^\w+ly$/i, tag => { // all the adverbs with which I start sentences...
    if (/channelfly/i.test(tag)) return 'channelfly'
    if (/italy/i.test(tag)) return 'italy'
    if (/barfly/i.test(tag)) return 'barfly'
    if (/fly/i.test(tag)) return tag
    console.log('stripping', tag)
    return ''
  })
  .replace(/^[ha ]+$/i, '')
  .replace(/^73$/i, '73')
  .replace(/^106/i, '106')
  .replace(/^476/i, '476')
  .replace(/^\d{1,3}$/i, '')
  .replace(/^gig$/i, '')
  .replace(/^bar$/i, '')
  .replace(/^s$/i, '')
  .replace(/^according/i, '')
  .replace(/^actual\w*/i, '')
  .replace(/^any\w*/i, '')
  .replace(/^another*/i, '')
  .replace(/^after$/i, '')
  .replace(/^assuming/i, '')
  .replace(/^anyway$/i, '')
  .replace(/^because/i, '')
  .replace(/^before$/i, '')
  .replace(/^booked/i, '')
  .replace(/^booze/i, '')
  .replace(/^celebrity/i, '')
  .replace(/^cheap/i, '')
  .replace(/^could/i, '')
  .replace(/^country/i, '')
  .replace(/^doing/i, '')
  .replace(/^djing/i, '')
  .replace(/^going/i, '')
  .replace(/^grandpa$/i, '')
  .replace(/^going/i, '')
  .replace(/^clare$/i, '')
  .replace(/^crazy$/i, '')
  .replace(/^every\W*/i, '')
  .replace(/^fantastic/i, '')
  .replace(/^finished/i, '')
  .replace(/^found/i, '')
  .replace(/^look\w+/i, '')
  .replace(/^hello/i, '')
  .replace(/^hm\w+/i, '')
  .replace(/^however/i, '')
  .replace(/^img\w+/i, '')
  .replace(/^living/i, '')
  .replace(/^local$/i, '')
  .replace(/^lover/i, '')
  .replace(/^manager/i, '')
  .replace(/^maybe$/i, '')
  .replace(/^might$/i, '')
  .replace(/^mighty$/i, '')
  .replace(/^number$/i, '')
  .replace(/^online/i, '')
  .replace(/^planned$/i, '')
  .replace(/^please/i, '')
  .replace(/^plus\W+/i, '')
  .replace(/^poor/i, '')
  .replace(/^quite/i, '')
  .replace(/^quiet/i, '')
  .replace(/^real/i, '')
  .replace(/\W+restaurant$/i, '')
  .replace(/^seems/i, '')
  .replace(/^several/i, '')
  .replace(/^should/i, '')
  .replace(/^starts\W+/i, '')
  .replace(/^still\W+/i, '')
  .replace(/^strange$/i, '')
  .replace(/^someone$/i, '')
  .replace(/^still/i, '')
  .replace(/^the\W*$/i, '')
  .replace(/^their/i, '')
  .replace(/^thank.*$/i, '')
  .replace(/^that.?s/i, '')
  .replace(/^there/i, '')
  .replace(/^thing/i, '')
  .replace(/^these/i, '')
  .replace(/^those/i, '')
  .replace(/^thou\w*/i, '')
  .replace(/^today$/i, '')
  .replace(/^tomorrow$/i, '')
  .replace(/^update$/i, '')
  .replace(/^what\w*/i, '')
  .replace(/^where$/i, '')
  .replace(/^would/i, '')
  // .replace(/^work/i, 'work')
  .replace(/^while/i, '')
  // .replace(/^whole$/i, '')
  .replace(/^venue/i, '')
  .replace(/^yesterday$/i, '')
  .replace(/^with\W+/i, '')
  .replace(/^monday\W*/i, '')
  .replace(/^tuesday\W*/i, '')
  .replace(/^wednesday\W*/i, '')
  .replace(/^thursday\W*/i, '')
  .replace(/^friday\W*/i, '')
  .replace(/^saturday\W*/i, '')
  .replace(/^sunday\W*/i, '')
  .replace(/^january\W*/i, '')
  .replace(/^february\W*/i, '')
  .replace(/^march\W*/i, '')
  .replace(/^april\W*/i, '')
  // .replace(/^may\W*/i, '')
  // .replace(/^june\W*/i, '')
  // .replace(/^july\W*/i, '')
  .replace(/^august\W*/i, '')
  .replace(/^september\W*/i, '')
  .replace(/^october\W*/i, '')
  .replace(/^november\W*/i, '')
  .replace(/^december\W*/i, '')
  .replace(/(\w+\s){5,}/i, tag => {
    if (/have i got news for/i.test(tag)) return 'have i got news for you'
    console.log({ tag }, 'too many spaces?')
    return ''
  })

const longerThan = exports.longerThan = len => tag => ('' + tag).length > len
const longEnough = exports.longEnough = longerThan(4)

const notEmpty = exports.notEmpty = tag => tag

const sensibleTags = exports.sensibleTags = (tag, text) => {
  if (/^s.m.a.s.h/i.test(tag)) return 'smash'
  tag = tag.replace(/ s\b/g, 's')
  if (/^o2/i.test(tag)) return 'o2'
  if (/^u2/i.test(tag)) return 'u2'
  if (/^qi/i.test(tag)) return 'qi'
  if (/^sim$/i.test(tag)) return 'sim card'
  if (/^hmv/i.test(tag)) return 'hmv'
  if (/^usa/i.test(tag)) return 'usa'
  if (/^usb/i.test(tag)) return 'usb'
  if (/^dice/i.test(tag)) return 'dice'
  if (/^iran/i.test(tag)) return 'iran'
  if (/^iraq/i.test(tag)) return 'iraq'
  if (/^popex/i.test(tag)) return 'popex'
  if (/parent:/.test(tag)) return
  if (tag === '"0"') return
  if (tag === '"b"') return 'popex'
  if (tag === 'stalking') return 'stalking heads'
  if (tag === '"20"') return 'stalking heads'
  if (tag === 'clare') return
  if (/hignf/i.test(tag)) return 'have i got news for you'
  // if (/^harry$/i.test(tag)) return
  // if (/^tommy$/i.test(tag)) return
  if (/ahhjoaad7c0eq9gy3lly5tmtjnzetymypjy0liipfm0/i.test(tag)) return
  if (/ypells_uq3xhf/i.test(tag)) return
  if (/yq4auh2npyc/i.test(tag)) return
  if (/yrn5c_mttbjblzducf77khxrjcloodpd2ubxvkitovdywnztq2rt98l3hphgyfteedsnl/i.test(tag)) return
  if (/yybwpmwfenwudyrpqa3hvx_uub7ikcbmj4safzykbry14m3yphhhw7ehnzvgcgvvqojlkhgadgwb96wsmgy0rq1icyzftw9mw0cts3ufzpnmfy/i.test(tag)) return
  if (/yypqoa8_lfq/i.test(tag)) return
  if (/z5_i8kpsvdt0ldotxakpuapo/i.test(tag)) return
  if (/zbmed6uuprz5akzs_e1azdmtjnzetymypjy0liipfm0/i.test(tag)) return
  if (/^b00/i.test(tag)) return
  if (/z\W?wave/i.test(tag)) return 'z wave'
  if (/^js/i.test(tag)) return 'javascript'
  if (/xbox/i.test(tag)) return 'xbox'
  if (/xss/i.test(tag)) return 'xss'
  if (/zingzilla/i.test(tag)) return 'zingzillas'
  if (/zizz/i.test(tag)) return 'zizzi'
  if (/zodiac mindwarp/i.test(tag)) return 'zodiac mindwarp'
  if (/adrian edmondson/i.test(tag)) return 'ade edmondson'
  if (/zomb/i.test(tag)) return 'zombies'
  if (/z00/i.test(tag)) return 'zoo thousand'
  if (/zo0/i.test(tag)) return 'zoo thousand'
  if (/zoo thousand/i.test(tag)) return 'zoo thousand'
  if (/zoo8/i.test(tag)) return 'zoo thousand'
  if (/zoo9/i.test(tag)) return 'zoo thousand'
  if (/zoothousand/i.test(tag)) return 'zoo thousand'
  if (/ztype/i.test(tag)) return
  if (/zufari/i.test(tag)) return
  if (/zwan/i.test(tag)) return
  if (/gorky/i.test(tag)) return 'gorkys zygotic mynci'
  if (/zygotic mynci/i.test(tag)) return 'gorkys zygotic mynci'
  if (/ac\W?dc/i.test(tag)) return 'ac dc'
  if (/indigo/i.test(tag)) return 'indigo'
  if (/leas cliffe hall/i.test(tag)) return 'leas cliff hall'
  if (/paul clarke/i.test(tag)) return 'pauly'
  if (/itunes/i.test(tag)) return 'itunes'
  if (/glasto/i.test(tag)) return 'glastonbury'
  if (/m.tley cr.e/i.test(tag)) return 'motley crue'
  if (/h.sker d./i.test(tag)) return 'husker du'
  if (/super ?furr/i.test(tag)) return 'super furry animals'
  if (/sfa/i.test(tag)) return 'super furry animals'
  if (/victorious/i.test(tag)) return 'victorious'
  if (/last.?fm/i.test(tag)) return 'last fm'
  if (/brian.+?may/i.test(tag)) return 'brian may'
  if (/belle.+?sebastian/i.test(tag)) return 'belle and sebastian'
  if (/lone *star comedy/i.test(tag)) return 'lone star comedy'
  if (/uncle bob/i.test(tag)) return 'uncle bobs' // yeah missing apostrophe
  if (/ambassador.+?reception/i.test(tag)) return 'ambassadors reception' // yeah missing apostrophe
  if (/water rat/i.test(tag)) return 'water rats' // again
  if (/frankie and benny/i.test(tag)) return 'frankie and bennys' // again
  if (/^morrison/i.test(tag)) return 'morrisons' // again
  if (/^mildred/i.test(tag)) return 'mildreds' // again
  if (/^gregg.s/i.test(tag)) return 'greggs' // again
  if (/hibbet/i.test(tag)) return 'mj hibbett'
  if (/buswerks/i.test(tag)) return 'og buswerks'
  if (/busworks/i.test(tag)) return 'og buswerks'
  if (/citaro/i.test(tag)) return 'bendy bus'
  if (/bendy bus/i.test(tag)) return 'bendy bus'
  if (/geotag/i.test(tag)) return 'geotag'
  if (/the darkness/i.test(tag)) return 'darkness'
  if (/micropub/i.test(tag)) return 'micropub'
  if (/bulgaria/i.test(tag)) return 'bulgaria'
  if (/kipp.?s/i.test(tag)) return 'kipps'
  if (/dennis the menace/i.test(tag)) return 'beano'
  if (/gnasher/i.test(tag)) return 'beano'
  if (/beano.?s/i.test(tag)) return 'beano'
  if (/cheese.*grater/i.test(tag)) return 'cheesegrater'
  if (/quarter.*house/i.test(tag)) return 'quarterhouse'
  if (/dolphin/i.test(tag)) return 'dolphin'
  if (/mushroom/i.test(tag)) return 'mushrooms'
  if (/squirrel/i.test(tag)) return 'squirrel'
  if (/magpie/i.test(tag)) return 'magpie'
  if (/park ?run/i.test(tag)) return 'parkrun'
  if (/kiss/i.test(tag)) return 'kiss'
  if (/paul stanley/i.test(tag)) return 'kiss'
  if (/gene simmons/i.test(tag)) return 'kiss'
  if (/donut/i.test(tag)) return 'doughnuts'
  if (/doughnut/i.test(tag)) return 'doughnuts'
  if (/holidayextras/i.test(tag)) return 'holiday extras'
  if (/mouse.?trap/i.test(tag)) return 'mousetrap'
  if (/mouse[^t]/i.test(tag)) return 'mouse' // not mousetrap
  if (/mice/i.test(tag)) return 'mouse'
  if (/^rats?$/i.test(tag)) return 'big mouse'
  if (/^waga/i.test(tag)) return 'wagamama'
  if (/bangladesh/i.test(tag)) return 'bangladesh'
  if (/nepal/i.test(tag)) return 'nepal'
  if (/korea/i.test(tag)) return 'korea'
  if (/germany/i.test(tag)) return 'germany'
  if (/german/i.test(tag)) return 'german'
  if (/ipod/i.test(tag)) return 'ipod'
  if (/wedge/i.test(tag)) return 'wedgewood rooms'
  if (/raymond.*timpkins/i.test(tag)) return 'raymond and mr timpkins'
  if (/astoria/i.test(tag)) return 'astoria'
  if (/manics/i.test(tag)) return 'manic street preachers'
  if (/^sleeping$/i.test(tag)) return 'sleep'
  if (/buffy/i.test(tag)) return 'buffy the vampire slayer'
  if (/botolph\W?s bridge/i.test(tag)) return 'botolphs bridge inn'
  if (/^surf/i.test(tag)) return 'surfing'
  if (/animal men/i.test(tag)) return 'these animal men'
  if (/canoe/i.test(tag)) return 'canoe'
  if (/temple of doom/i.test(tag)) return 'indiana jones'
  if (/raiders of the lost ark/i.test(tag)) return 'indiana jones'
  if (/record prod/i.test(tag)) return 'producers'
  if (/^sbm/i.test(tag)) return 'producers'
  if (/stephen budd/i.test(tag)) return 'producers'
  if (/elbow room/i.test(tag)) return 'elbow rooms'
  if (/shepway test/i.test(tag)) return 'kent test'
  if (/bid+enden/i.test(tag)) return 'biddenden'
  if (/burglar/i.test(tag)) return 'burglary'
  if (/tesco/i.test(tag)) return 'tesco'
  if (/waitrose/i.test(tag)) return 'waitrose'
  if (/^morrison/i.test(tag)) return 'morrison'
  if (/sainsbury/i.test(tag)) return 'sainsbury'
  if (/^wilko johnson/i.test(tag)) return 'wilko johnson'
  if (/^wilkinson/i.test(tag)) return 'wilkinson'
  if (/^wilko/i.test(tag)) return 'wilkinson'
  if (/^nahid/i.test(tag)) return 'nahidz'
  if (/^luben/i.test(tag)) return 'luben'
  if (/printer/i.test(tag)) return 'printer'
  if (/micro\W?scooter/i.test(tag)) return 'microscooter'
  if (/mobility\W?scooter/i.test(tag)) return 'mobility scooter'
  if (/scooter/i.test(tag)) return 'scooter'
  if (/hair ?cut/i.test(tag)) return 'haircut'
  if (/legoland/i.test(tag)) return 'legoland'
  if (/lego/i.test(tag)) return 'lego'
  if (/^sick/i.test(tag)) return 'sick'
  if (/cullen.*yard/i.test(tag)) return 'cullins yard'
  if (/cullin.*yard/i.test(tag)) return 'cullins yard'
  if (/cataract/i.test(tag)) return 'cataracts'
  if (/hop ?fuzz/i.test(tag)) return 'hopfuzz'
  if (/crossbow/i.test(tag)) return 'archery'
  if (/car ?boot sale/i.test(tag)) return 'boot fair'
  if (/boot ?fair/i.test(tag)) return 'boot fair'
  if (/country ?warmth/i.test(tag)) return 'country warmth'
  if (/new year/i.test(tag)) return 'new year'
  if (/big brother/i.test(tag)) return 'big brother'
  if (/trail of dead/i.test(tag)) return 'and you will know us by the trail of dead'
  if (/arrabiata/i.test(tag)) return 'arrabiata'
  if (/terrorvision/i.test(tag)) return 'terrorvision'
  if (/terror/i.test(tag)) return 'terrorism'
  if (/corona ?virus/i.test(tag)) return 'coronavirus'
  if (/book week/i.test(tag)) return 'world book day'
  if (/book day/i.test(tag)) return 'world book day'
  if (/cruis.*blast/i.test(tag)) return 'cruis n blast'
  if (/^gotham/i.test(tag)) return 'gotham'
  if (/^goth/i.test(tag)) return 'goth'
  if (/de *haan/i.test(tag)) return 'roger de haan'
  if (/iron ?man/i.test(tag)) return 'iron man'
  if (/bat ?man/i.test(tag)) return 'batman'
  if (/dog ?man/i.test(tag)) return 'dog man'
  if (/wetherspoon/i.test(tag)) return 'wetherspoons'
  if (/albert hall/i.test(tag)) return 'albert hall'
  if (/harvey ?grammar/i.test(tag)) return 'harvey grammar'
  if (/redundan/i.test(tag)) return 'redundancy'
  if (/eccleston/i.test(tag)) return 'christopher eccleston'
  if (/google wave/i.test(tag)) return 'google wave'
  if (/mikku/i.test(tag)) return 'mikku powder'
  if (/wonderstuff/i.test(tag)) return 'wonder stuff'
  if (/seabrook/i.test(tag)) return 'seabrook'
  if (/gary glitter/i.test(tag)) return 'gary glitter'
  if (/namco/i.test(tag)) return 'namco'
  if (/westminster/i.test(tag)) return 'westminster'
  if (/barfly/i.test(tag)) return 'barfly'
  if (/coldplay/i.test(tag)) return 'coldplay'
  if (/haybittle/i.test(tag)) return 'agnes haybeetle'
  if (/haybeetle/i.test(tag)) return 'agnes haybeetle'
  if (/darts/i.test(tag)) return 'darts'
  if (/dartboard/i.test(tag)) return 'darts'
  if (/dart board/i.test(tag)) return 'darts'
  if (/metall?ica/i.test(tag)) return 'metallica'
  if (/dyson/i.test(tag)) return 'dyson'
  if (/hand dryer/i.test(tag)) return 'hand dryer'
  if (/bunkbed/i.test(tag)) return 'bunk beds'
  if (/bunkbeds/i.test(tag)) return 'bunk beds'
  if (/bunk bed/i.test(tag)) return 'bunk beds'
  if (/leicester sq/i.test(tag)) return 'leicester square'
  if (/leicester/i.test(tag)) return 'leicester'
  if (/nottingham/i.test(tag)) return 'nottingham'
  if (/leeds/i.test(tag)) return 'leeds'
  if (/birmingham/i.test(tag)) return 'birmingham'
  if (/bath/i.test(tag)) return 'bath'
  if (/fowa/i.test(tag)) return 'fowa'
  if (/html/i.test(tag)) return 'html'
  if (/propert/i.test(tag)) return 'property'
  if (/jquery/i.test(tag)) return 'jquery'
  if (/jambo/i.test(tag)) return 'will mellor'
  if (/keira/i.test(tag)) return 'keira knightley'
  if (/radio4/i.test(tag)) return 'radio 4'
  if (/radio four/i.test(tag)) return 'radio 4'
  if (/solar power/i.test(tag)) return 'solar power'
  if (/solar panel/i.test(tag)) return 'solar power'
  if (/security camera/i.test(tag)) return 'security camera'
  if (/quiz/i.test(tag)) return 'quiz'
  if (/stoke newington/i.test(tag)) return 'stoke newington'
  if (/ringtone/i.test(tag)) return 'ringtone'
  if (/bookmarklet/i.test(tag)) return 'bookmarklet'
  if (/spirit wife/i.test(tag)) return 'david devant'
  if (/unstoppable sex machine/i.test(tag)) return 'carter usm'
  if (/^carter$/i.test(tag)) return 'carter usm'
  if (/donington/i.test(tag)) return 'donington'
  if (/facebook/i.test(tag)) return 'facebook'
  if (/habitat radius/i.test(tag)) return 'habitat radius'
  if (/^habitat/i.test(tag)) return 'habitat'
  if (/^electrics?$/i.test(tag)) return 'electricity'
  if (/freecyc/i.test(tag)) return 'freecycle'
  if (/hoarder/i.test(tag)) return 'hoarding'
  if (/hoarded/i.test(tag)) return 'hoarding'
  if (/hoarding/i.test(tag)) return 'hoarding'
  if (/gulliver.*kingd/i.test(tag)) return 'gullivers kingdom'
  if (/gulliver'?s/i.test(tag)) return 'gullivers kingdom'
  if (/pool table/i.test(tag)) return 'pool table'
  if (/banksy/i.test(tag)) return 'banksy'
  if (/baby bird/i.test(tag)) return 'baby bird'
  if (/baby/i.test(tag)) return 'baby'
  if (/britain.* got talent/i.test(tag)) return 'britains got talent'
  if (/naboo/i.test(tag)) return 'naboo'
  if (/julian ?barr?[ae]t/i.test(tag)) return 'julian barratt'
  if (/mighty ?boosh/i.test(tag)) return 'mighty boosh'
  if (/internet expl/i.test(tag)) return 'ie'
  if (/google ?map/i.test(tag)) return 'google maps'
  if (/headtorch/i.test(tag)) return 'head torch'
  if (/scrobbl/i.test(tag)) return 'scrobbling'
  if (/livin' on a prayer/i.test(tag)) return 'livin on a prayer'
  if (/dry january/i.test(tag)) return 'dryanuary'
  if (/if this then that/i.test(tag)) return 'ifttt'
  if (/ifttt/i.test(tag)) return 'ifttt'
  if (/lightwaverf/i.test(tag)) return 'lightwaverf'
  if (/laura ashley milton/i.test(tag)) return 'laura ashley milton'
  if (/stellastar/i.test(tag)) return 'stellastarr'
  if (/brew ?dog/i.test(tag)) return 'brewdog'
  if (/^ns i/i.test(tag)) return 'premium bonds'
  if (/national savings and investments/i.test(tag)) return 'premium bonds'
  if (/^ernie$/i.test(tag)) return 'premium bonds'
  if (/^e r n i e/i.test(tag)) return 'premium bonds'
  if (/nsandi/i.test(tag)) return 'premium bonds'
  if (/premium bond/i.test(tag)) return 'premium bonds'
  if (/(vw|volkswagen) ?(camper|van|bus)/i.test(tag)) return 'vw camper'
  if (/kosso/i.test(tag)) return 'kosso'
  if (/devant/i.test(tag)) return 'david devant'
  if (/pwei/i.test(tag)) return 'pop will eat itself'
  if (/pirate/i.test(tag)) return 'pirates'
  if (/strictly/i.test(tag)) return 'strictly come dancing'
  if (/gbbo/i.test(tag)) return 'great british bake off'
  if (/bake off/i.test(tag)) return 'great british bake off'
  if (/^time travel/i.test(tag)) return 'time travel'
  if (/van life/i.test(tag)) return 'vanlife'
  if (/^podcast/i.test(tag)) return 'podcast'
  if (/qotsa/i.test(tag)) return 'queens of the stone age'
  if (/suede/i.test(tag)) return 'suede'
  if (/r\W+e\W+m/i.test(tag)) return 'rem'
  if (/r\W*e\W*m\W+/i.test(tag)) return 'rem'
  if (/5\W2/i.test(tag)) return '5 2 diet'
  if (/lanterns/i.test(tag)) return 'lanterns'
  if (/herring/i.test(tag)) return 'richard herring'
  if (/zombie/i.test(tag)) return 'zombies'
  if (/jamaica/i.test(tag)) return 'jamaica'
  if (/xfm/i.test(tag)) return 'xfm'
  if (/zizzi/i.test(tag)) return 'zizzi'
  if (/\$old out/i.test(tag)) return 'sold out'
  if (/^.old out/i.test(tag)) return 'sold out'
  if (/goldie look/i.test(tag)) return 'goldie lookin chain'
  if (/zodiac mindwarp/i.test(tag)) return 'zodiac mindwarp'
  if (/center ?parcs/i.test(tag)) return 'center parcs'
  if (/centre ?parcs/i.test(tag)) return 'center parcs'
  if (/^chili/i.test(tag)) return 'chilli'
  if (/^"?node/i.test(tag)) return 'nodejs'
  if (/^express\s?js/i.test(tag)) return 'express'
  if (/433mhz/i.test(tag)) return '433mhz'
  if (/^npm/i.test(tag)) return 'npm'
  if (/^work\w*( from home)/i.test(tag)) return 'wfh'
  if (/^work/i.test(tag)) return 'work'
  if (/^remote control/i.test(tag)) return 'remote control'
  return tag.replace(/-/g, ' ').replace(/'s(\s|\b|$)/g, (all, space) => {
    console.log('stripping apostrophe?', { all, space })
    return `s${space}`
  })
}

const frontMatterTags = exports.frontMatterTags = text => {
  const regex = /tags: \[(.+)\]/
  let tags = ('' + text).match(regex)
  tags = ((tags && tags[1]) || '').split(/, */).filter(removeCommonWords)
    .filter(longerThan(1))
    .map(sensibleTags)
    .filter(notEmpty)
  const stalkingHeads = ('' + text).match(/board: 16/)
  if (stalkingHeads) {
    tags.push('popex')
    tags.push('stalking heads')
  }
  const match = /date: (.+)/.exec(text)
  if (match) {
    if (match[1] > '1999' && match[1] < '2003-05-08') {
      tags.push('popex')
      console.warn({ match })
    }
  }
  tags = tags.filter(tag => !['club', 'closed', 'derelict', 'food', 'other', 'child friendly', 'garden', 'music', 'travel', 'transport', 'pub'].includes(tag))
  return [...new Set(tags)]
}

exports.extractTagArray = text => {
  const oldStyleTags = (('' + text).match(/\[\[[\w\s]+\]\]/g) || [])
    .map(foo => urlify(foo.replace(/[[\]]/g, '')))
    .map(removeCommonWords)
    .filter(longEnough)
    .filter(notEmpty)
  // const textWithoutBlockQuote = ('' + text).replace(/<blockquote[\s\S]+blockquote>/gi, '')
  // const fromCaps = (('' + textWithoutBlockQuote).match(/(\b[A-Z]\w+('s)?(\s|\b))+/g) || [])
  text = ('' + text)
    .replace(/]\(.+?\)/g, ']')
    .replace(/^.*?(\|\s+\S.*?){2,}.*$/mg, tableRow => {
      console.log('table?', tableRow)
      return ''
    })
  const fromCaps = (('' + text).match(/(\b[A-Z]\w+('s)?(\s|\b))+/g) || [])
    .map(tag => tag.trim())
    .map(removeCommonWords)
    .filter(longEnough)
    .filter(notEmpty)
  const newTags = frontMatterTags(text)
  return oldStyleTags.concat(fromCaps, newTags)
    .map(tag => tag.trim().toLowerCase())
    .reduce(exports.unique, [])
    .map(sensibleTags)
    .filter(notEmpty)
}

exports.extractTags = text => exports.extractTagArray(text).map(urlify).join(' ')

const slugify = exports.slugify = tag => urlify(tag)
  .replace(/'s$/g, '')
  .replace(/\W+/g, '-')

const hasNestedLinks = text => {
  // an open [ followed by another open [ before a close ]
  return /\[[^\]]*\[/.test(text)
}

const byLength = (a, b) => {
  if (String(a).length < String(b).length) return 1
  if (String(b).length < String(a).length) return -1
  return 0
}

exports.addTags = (text, tags) => {
  if (hasNestedLinks(text)) {
    console.warn({ text }, 'already has nested tags, fix this')
    return text
  }
  // console.log({ text, tags })
  return tags.sort(byLength).reduce((text, tag) => {
    if (!tag) return text
    const regex = new RegExp(`([\\s^])(the )?(${tag}\\w*?)([\\s.,;:$!?)]|'s)`, 'gi')
    const newText = text.replace(regex, function (match, firstBarrier, optionalThe, word, secondBarrier) {
      word = (optionalThe || '') + word
      return firstBarrier + wikify(word, tag) + secondBarrier
    })
    if (hasNestedLinks(newText)) {
      console.warn({ tag, newText }, 'would cause nested tags, so skip')
      return text
    }
    return newText
  }, text)
}

const wikify = exports.wikify = (word, tag) => `[${word}](/wiki/#${slugify(tag || word).replace(/-/g, '/')})`

const linkify = (word, tag) => {
  // if (/david/i.test(word)) console.warn({ word, tag }, 'so make', `<a href="/wiki/#${slugify(tag || word).replace(/-/g, '/')}">${word}</a>`)
  return `<a href="/wiki/#${slugify(tag || word).replace(/-/g, '/')}">${word}</a>`
}

const quote = exports.quote = text => {
  text = printable(text).trim()
  if (/^[a-z][\w\s]+$/i.test(text)) { // simple words don't need quotes
    // if (!/^\d+$/i.test(text)) { // unless it is all numbers
    return text
  }
  if (/^"[^"]*"$/i.test(text)) { // if it is already quoted
    return text
  }
  return JSON.stringify(text)
}

exports.htmlify = text => {
  return ('' + text)
    // todo more here including images
    .replace(/{%Tag1%}/g, '') // weird ticketmaster hing
    .replace(/\[\*\]/g, '+')
    .replace(/\[@\w+\]([\s\S]+?)\[\/@\w+\]/g, (foo, content) => `{# ${content.replace(/\s+/g, ' ')} #}`)
    .replace(/\[\[(http.+?) ([^\]|]+) \| (.+?)\]\]/g, (foo, url, text, title) => `<a href="${url}" title="${title}">${text}</a>`)
    .replace(/\[\[(http[^\]|]+?) ([^\]|]+)\]\]/g, (foo, url, text) => `<a href="${url}">${text}</a>`)
    .replace(/\[\[(\w+:\/\/)(www\.)?(.+?)\]\]/g, (foo, protocol, subdomain, rest) => `<a href="${protocol}${subdomain || ''}${rest}">${rest}</a>`)
    .replace(/\[url\](\w+:\/\/)(www\.)?([^[]+)\[\/url\]/g, (foo, protocol, subdomain, rest) => `<a rel="nofollow noopener" href="${protocol}${subdomain || ''}${rest}">${rest}</a>`)
    .replace(/\[url=(\w+:\/\/)(www\.)?([^\]]+)\]([^\]]+)\[\/url\]/g, (foo, protocol, subdomain, rest, text) => `<a rel="nofollow noopener" href="${protocol}${subdomain || ''}${rest}">${text}</a>`)
    .replace(/\[img\](.+?)\[\/img\]/g, (foo, rest) => `<img src="${rest}" />`)
    .replace(/\[name\](.+?)\[\/name\]/g, (foo, name) => `<a href="/names/${urlify(name)}">${name}</a>`)
    .replace(/\[quote\]([\s\S]+?)\[\/quote\]/g, (foo, content) => `<blockquote>${content}</blockquote>`)
    .replace(/\[youtube\](.+?)\[\/youtube\]/g, (foo, content) => `<iframe title="YouTube video player" width="800" height="600" src="http://www.youtube.com/embed/${content}?hd=1" frameborder="0" allowfullscreen></iframe>`)
    // .replace(/\[include\](.+?)\[\/include\]/g, (foo, content) => `{% include ${slugify(content)}.html %}`)
    .replace(/\[\[(198[789])\]\]/g, (foo, year) => `<a href="/misc/gigography/${year}/">${year}</a>`)
    .replace(/\[\[(199\d)\]\]/g, (foo, year) => `<a href="/misc/gigography/${year}/">${year}</a>`)
    .replace(/\[\[(20[01]\d)\]\]/g, (foo, year) => `<a href="/misc/gigography/${year}/">${year}</a>`)
    // .replace(/\[\[(.+?)\]\]/g, (foo, tag) => wikify(tag))
    // .replace(/<a href="\/wiki\/([^#")">([^<]+)<\/a>/g, (foo, tag, word) => linkify(word, tag))
    .replace(/\[\[(.+?)\]\]/g, (foo, tag) => linkify(tag))
    .replace(/\[fg\](.+?)\[\/fg\]/g, (foo, tag) => `<a href="http://www.folkestonegerald.com/wiki/${urlify(tag)}">${tag}</a>`)
    .replace(/\[gigography\](.+?)\[\/gigography\]/g, (foo, tag) => `<a href="/misc/gigography/${urlify(tag)}">${tag}</a>`)
    .replace(/\[b\](.+?)\[\/b\]/g, (foo, tag) => `<b>${tag}</b>`)
    .replace(/\[i\](.+?)\[\/i\]/g, (foo, tag) => `<i>${tag}</i>`)
    .replace(/\[artist\](.+?)\[\/artist\]/g, (foo, tag) => linkify(tag))
    .replace(/\[place\](.+?)\[\/place\]/g, (foo, tag) => linkify(tag))
    .replace(/\[artist=([^\]]+)\](.+?)\[\/artist\]/g, (foo, tag, text) => linkify(tag, text))
    .replace(/\[place=([^\]]+)\](.+?)\[\/place\]/g, (foo, tag, text) => linkify(tag, text))
    .replace(/\[google\](.+?)\[\/google\]/g, (foo, tag) => `<a href="https://www.google.co.uk/search?q=${urlify(tag)}">${tag}</a>`)
    // .replace(/\[(\w+)\](.+?)\[\/\1+\]/g, (foo, tag, content) => `<a class="🚮${tag} ${content}" href="/wiki/${urlify(content)}">${content}</a>`) // catch all
    // .replace(/\s+/g, ' ')
    .replace(/[^ -~i\x0a\x9c]/g, '') // eslint-disable-line
    .trim()
}

exports.ymlsafe = text => ('' + text).replace(/[\r\n]/g, '<br />')

exports.replaceRelativeFile = (file, content, callback) => {
  const fullPath = path.join(__dirname, file)
  replaceFile(fullPath, content, callback)
}

const replaceFile = exports.replaceFile = (fullPath, content, callback) => {
  fs.readFile(fullPath, 'utf8', (ignoredError, original) => {
    if (original === content) return callback() // nothing changed
    console.log('🆕', fullPath)
    if (/^---/.test(content)) {
      // it's got frontmatter
      const originalWithoutFrontMatter = String(original).replace(/[\s\S]+?^---$/m, '')
      const contentWithoutFrontMatter = content.replace(/[\s\S]+?^---$/m, '')
      if (originalWithoutFrontMatter !== contentWithoutFrontMatter) {
        console.log({ originalWithoutFrontMatter, contentWithoutFrontMatter })
        content = content.replace(/lastmod: .+\n/, '')
        const lastmod = new Date().toISOString().substr(0, 10)
        const match = /^date: (.+)$/m.exec(content)
        if (match) {
          const date = new Date(match[1]).toISOString().substr(0, 10)
          if (date !== lastmod) {
            content = content.replace(/^---$/m, `---\nlastmod: ${lastmod}`)
          }
        }
      }
    }
    mkdirp(path.dirname(fullPath), () => {
      fs.writeFile(fullPath, content, 'utf8', callback)
    })
  })
}

// turn an array of tags into an array of tasks for async
exports.makeTagTasks = occurences => {
  return Object.keys(occurences).map(tag => {
    return callback => {
      if (occurences[tag] === 1) {
        console.error(`grep -irl "${tag.replace(/\+/g, ' ')}" _posts gig | xargs -o vi`)
        return callback()
      }
      if (tag.length < MINTAGLENGTH) {
        console.error(`grep -irlE "tags: .*${tag.replace(/\+/g, ' ')}" _posts gig | xargs -o vi`)
        return callback()
      }
      const content = `---
layout: wiki
title: ${quote(tag.trim())}
tags: [${quote(tag.trim())}]
---`
      replaceFile(`../wiki/${urlify(tag)}/index.html`, content, callback)
    }
  })
}

exports.unique = (arr, val) => {
  if (!arr.includes(val)) arr.push(val)
  return arr
}
